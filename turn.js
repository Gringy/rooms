const go = require("./gameobject");
const abilities = require("./abilities");
const events = require("./commands");

/**
 * Represents turn of turning object
 */
class Turn {
	/**
	 * 
	 * @param {go.GameObject} object 
	 */
    constructor(object) {
        this.object = object;
        this.turnAbility = object.getAbility(abilities.TurnAbility);
        if (this.turnAbility == null)
            throw new TypeError("Turn.ctor: object has no TurnAbility");
        this.MP = this.turnAbility.maxMP;
        this.AP = this.turnAbility.maxAP;
        this.timeAvailable = 60;
        this.command = null;
        this.context = {};
        this.firstTick = true;
    }

    tick() {
        if (this.firstTick) {
            this.firstTick = false;
            if (this.object.isDead()) {
                this.turnAbility.dead();
                return;
            }
            this.turnAbility.statsChanged(this.MP, this.AP);
        }
        if (this.timeAvailable <= 0) {
            this.object.room.endTurn();
            return;
        }
        if (this.timeLeft < 0) {
            if (this.command != null)
                this.command.postPerform();
            this.command = null;
        }
        if (this.command == null) {
            const command = this.turnAbility.turn(this);
            if (command != null
                && this.MP >= command.MP
                && this.AP >= command.AP
                && this.timeAvailable >= command.time) {

                this.MP -= command.MP;
                this.AP -= command.AP;
                this.turnAbility.statsChanged(this.MP, this.AP);
                this.command = command;
                this.timeLeft = command.time;
                command.perform();
            }
        }
        this.timeAvailable--;
        this.timeLeft--;
    }

    get id() {
        return this.object.id;
    }

    /**
     * Returns new turn with refreshed MP, AP and time
     * 
     * @returns {Turn} A new turn
     */
    next() {
        const nextTurnTime = Math.min(this.timeAvailable + 30, 60);
        const nextTurn = new Turn(this.object);
        nextTurn.timeAvailable = nextTurnTime;
        return nextTurn;
    }
}

exports.Turn = Turn;