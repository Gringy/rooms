const ws = require("ws");

class Net {
	constructor(port) {
		this.server = new ws.Server({ port: port });
	}
	async getConnection() {
		return new Promise((resolve, reject) => {
			const errorCallback = (error) => {
				reject(error);
			};
			const connectionCallback = (ws, req) => {
				this.server.removeListener("error", errorCallback);
				resolve(new Conn(ws, req));
			};
			this.server.once("connection", connectionCallback);
			this.server.once("error", errorCallback);
		});
	}
}

class Conn {
	/**
	 * 
	 * @param {ws} ws 
	 * @param {*} request 
	 */
	constructor(ws, request) {
		this.ws = ws;
		this.request = request;
	}
	get address() {
		return this.request.connection.remoteAddress;
	}
	async read() {
		return new Promise((resolve, reject) => {
			const errorCallback = (error) => {
				console.log("Conn.read error: ", error);
				reject(error);
			};
			const closeCallback = (code, reason) => {
				reject(new Error("Conn.read: closed: " + code + " " + reason));
			};
			const messageCallback = (message) => {
				this.ws.removeListener("error", errorCallback);
				this.ws.removeListener("close", closeCallback);
				resolve(message);
			};
			this.ws.once("message", messageCallback);
			this.ws.once("error", errorCallback);
			this.ws.once("close", closeCallback);
		})
	}
	/**
	 * 
	 * @param {string} data 
	 * 
	 * @returns {Promise<void>}
	 */
	async write(data) {
		return new Promise((resolve, reject) => {
			const errorCallback = (error) => {
				console.log("Conn.write error: ", error);
				reject(error);
			};
			const closeCallback = (code, reason) => {
				reject(new Error("Conn.read: closed: " + code + " " + reason));
			};
			const resolveCallback = () => {
				this.ws.removeListener("error", errorCallback);
				this.ws.removeListener("close", closeCallback);
				resolve();
			}
			this.ws.send(data, {}, resolveCallback);
			this.ws.once("error", errorCallback);
			this.ws.once("close", closeCallback);

		});
	}
	close() {
		this.ws.close();
	}
}

exports.Net = Net;
exports.Conn = Conn;
