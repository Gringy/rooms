import { Renderer } from "./renderer";
import { State, GameObject } from "./state";
import { Gui } from "./gui";
import { Conn } from "./net";

export class Game {
    constructor() {
        const canvas = document.getElementById("canvas");
        this.app = new PIXI.Application({
            view: canvas,
            width: canvas.clientWidth,
            height: canvas.clientHeight
        });
        this.renderer = new Renderer(this.app, this);
        this.gui = new Gui(this.app, this, {
            loginCallback: (login, password) => {
                this.conn = new Conn(this, "ws://192.168.0.107:8086/", login, password);
                this.gui.setLoginVisible(false);
                this.gui.setPanelVisible(true);
            }
        })
        this.running = true;
    }
    message(msg) {
        console.log(msg);
        switch (msg.type) {
            case "init": {
                //console.log(msg);
                const room = msg.room;
                const state = new State(room.width, room.height, msg.id);
                for (const node in room.nodes) {
                    state.fillNode(node, room.nodes[node]);
                }
                for (const obj of room.objects) {
                    state.addObject(new GameObject(obj.id, obj.x, obj.y, obj.look));
                }
                state.setTurns(room.turns);
                this.state = state;
                this.renderer.state = state;
                this.renderer.onChangeState();
                this.gui.updateTurns(this.state);
                this.gui.updateStats(this.state);
                break;
            }
            case "move": {
                const object = this.state.findObjectById(msg.id);
                if (object != null) {
                    let [fx, fy] = msg.from;
                    let [tx, ty] = msg.to;
                    this.renderer.onMove(msg.id, fx, fy, tx, ty);
                    object.x = tx;
                    object.y = ty;
                }
                break;
            }
            case "add_object": {
                this.state.turns = msg.turns;
                const object = new GameObject(msg.object.id, msg.object.x, msg.object.y, msg.object.look);
                this.state.addObject(object);
                this.renderer.onCreate(object);
                this.gui.updateTurns(this.state);
                break;
            }
            case "turn_end": {
                this.state.turns = msg.turns;
                this.gui.updateTurns(this.state);
                this.gui.updateStats(this.state);
                break;
            }
            case "stats": {
                if (msg.MP != null)
                    this.state.MP = msg.MP;
                if (msg.AP != null)
                    this.state.AP = msg.AP;
                if (msg.HP != null)
                    this.state.HP = msg.HP;
                this.gui.updateStats(this.state);
                break;
            }
            case "leave": {
                this.renderer.onDelete(msg.id);
                this.state.removeObject(msg.id);
                this.state.turns = msg.turns;
                this.gui.updateStats(this.state);
                break;
            }
            case "hit": {
                this.renderer.onHit(msg.dst, msg.damage);
                break;
            }
            case "change": {
                this.renderer.onChangeLook(msg.id, msg.look);
                break;
            }
        }
    }
    closed() {
        this.state = null;
        this.renderer.state = null;
        this.running = false;
        this.conn.close();
        this.gui.setLoginVisible(true);
        this.gui.setPanelVisible(false);
    }
    send(obj) {
        if (this.conn != null)
            this.conn.ws.send(JSON.stringify(obj));
    }
    moveTo(x, y) {
        const player = this.state.getPlayer();
        if (player == null)
            return;
        console.log("move to ", x - player.x, y - player.y);
        this.send({
            type: "move_to",
            dx: x - player.x,
            dy: y - player.y
        })
    }

    hit(id) {
        this.send({
            type: "hit",
            id: id
        });
    }

    skipTurn() {
        this.send({
            type: "end_turn"
        });
    }

    move(dx, dy) {
        this.send({
            type: "move_to",
            dx: dx,
            dy: dy
        });
    }
}

