export class Conn {
    constructor(game, url, login, password) {
        this.ws = new WebSocket(url);
        this.ws.onopen = () => {
            console.log("opened");
            this.ws.send(JSON.stringify({
                login: login,
                password: password
            }))
        };
        this.ws.onclose = () => {
            console.log("closed");
            game.closed();
        };
        this.ws.onerror = () => {
            console.log("error");
        };
        this.ws.onmessage = (msg) => {
            const parsed = JSON.parse(msg.data);
            // console.log(parsed);
            game.message(parsed);
        };
    }
    close() {
        this.ws.close();
    }
}