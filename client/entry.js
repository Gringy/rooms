import { Game } from "./game";

addEventListener("load", () => {
    const game = new Game();

    // for debugging
    window.game = game;
});