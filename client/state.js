export class State {
    constructor(width, height, id) {
        this.width = width;
        this.height = height;
        this.playerId = id;
        this.nodes = new Array(width * height);
        this.objects = [];

        this.HP = 0;
        this.MP = 0;
        this.AP = 0;
    }
    getNode(x, y) {
        return this.nodes[y * this.width + x];
    }
    setNode(x, y, node) {
        this.nodes[y * this.width + x];
    }
    fillNode(n, node) {
        this.nodes[n] = node;
    }
    addObject(object) {
        this.objects.push(object);
    }
    removeObject(id) {
        for (let index in this.objects)
            if (this.objects[index].id === id) {
                this.objects.splice(index, 1);
                return;
            }
    }
    findObject(x, y) {
        for (const object of this.objects)
            if (object.x === x && object.y === y)
                return object;
        return null;
    }
    findObjectById(id) {
        for (const object of this.objects)
            if (object.id === id)
                return object;
        return null;
    }
    getPlayer() {
        return this.findObjectById(this.playerId);
    }
    setTurns(turns) {
        this.turns = turns;
    }
}

export class GameObject {
    constructor(id, x, y, look) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.look = look;
    }
}