import * as PIXI from "pixi.js";
import { MoveAction, HitAction } from "./actions";

export class Renderer {
    constructor(app, game) {
        this.app = app;
        this.game = game;
        this.textures = {};
        this.actions = [];

        this.tileW = 32;
        this.tileH = 32;

        PIXI.loader
            .add('pixel', 'assets/tileset.png')
            .add("toolbar", "assets/toolbar.png")
            .load((loader, resources) => {
                this.addTextures(resources.pixel.texture);
                this.container = new PIXI.Container();
                this.container.x = this.app.renderer.width / 2;
                this.container.y = this.app.renderer.height / 2;
                app.stage.addChild(this.container);
                if (this.state != null)
                    this.onChangeState();
                app.ticker.add((delta) => {
                    const elapsed = app.ticker.elapsedMS;
                    this.updateObjects(elapsed);
                });
            });
    }
    updateObjects(elapsed) {
        const newActions = [];
        for (const action of this.actions) {
            newActions.push(...action.perform(elapsed));
        }
        this.actions = newActions;
        this.updateContainerPosition();
    }
    addAction(action) {
        this.actions.push(action);
    }
    addUniqueAction(id, action) {
        for (let index in this.actions)
            if (this.actions[index].id === id) {
                this.actions[index] = action;
                return;
            }
        this.actions.push(action);
    }
    addTextures(texture) {
        this.addTexture("NONE", texture, 0, 0);
        this.addTexture("FLOOR", texture, 1, 0);
        this.addTexture("WOOD_FLOOR", texture, 13, 0);
        this.addTexture("UNDERGRASS", texture, 2, 0);
        this.addTexture("GRASS", texture, 3, 0);
        this.addTexture("WALL", texture, 4, 0);
        this.addTexture("BOOKS", texture, 0, 1);
        this.addTexture("STAIRS", texture, 8, 0);
        this.addTexture("DOOR", texture, 5, 0);
        this.addTexture("OPEN_DOOR", texture, 6, 0);
        this.addTexture("PLAYER", texture, 0, 2);
        this.addTexture("TREE", texture, 3, 2);
        this.addTexture("CRAB", texture, 7, 2);
        this.addTexture("CORPSE", texture, 1, 2);
    }
    createSprite(name, x, y) {
        const result = new PIXI.Sprite(this.textures[name]);
        result.x = x;
        result.y = y;
        result.anchor.x = 0.5;
        result.anchor.y = 0.5;
        return result;
    }
    getObjectSpriteName(id) {
        return "object[" + id + "]";
    }
    createObjectSprite(id, name, x, y) {
        const sprite = this.createSprite(name, x, y);
        sprite.name = this.getObjectSpriteName(id);
        sprite.interactive = true;
        sprite.on("pointerdown", () => {
            this.game.hit(id);
        });
        return sprite;
    }
    findObjectSprite(id) {
        const name = this.getObjectSpriteName(id);
        return this.container.getChildByName(name);
    }
    addTexture(name, texture, col, row) {
        const result = new PIXI.Texture(texture, new PIXI.Rectangle(col * this.tileW, row * this.tileH, this.tileW, this.tileH));
        this.textures[name] = result;
    }
    clearContainer() {
        this.container.removeChildren();
    }
    createContainer() {
        this.clearContainer();
        const state = this.state;

        if (state == null)
            return;
        if (this.textures.FLOOR == null)
            return;
        for (let x = 0; x < state.width; x++)
            for (let y = 0; y < state.height; y++) {
                const displayX = x;
                const displayY = y;
                const node = state.getNode(x, y);
                if (node == null) continue;
                const texture = this.textures[node];
                if (texture == null) {
                    console.log("texture=null" + " node=" + node);
                    continue;
                }
                const nodeSprite = new PIXI.Sprite(texture);
                nodeSprite.name = "node[" + x + "," + y + "]";
                nodeSprite.anchor.set(0.5);
                nodeSprite.x = displayX * this.tileW;
                nodeSprite.y = -displayY * this.tileH;
                nodeSprite.interactive = true;
                nodeSprite.on("pointerdown", () => {
                    this.game.moveTo(x, y);
                })
                this.container.addChild(nodeSprite);
            }
        for (let object of state.objects) {
            this.onCreate(object);
        }
    }
    set state(value) {
        this._state = value;
        this.createContainer();
    }
    get state() {
        return this._state;
    }
    updateContainerPosition() {
        if (this.state == null)
            return;
        const playerSprite = this.findObjectSprite(this.state.playerId);
        if (playerSprite == null)
            return;
        this.container.x = this.app.renderer.width / 2 - playerSprite.x;
        this.container.y = this.app.renderer.height / 2 - playerSprite.y;
    }
    onChangeState() {
        this.actions = [];
        this.createContainer();
        this.updateContainerPosition();
    }
    onMove(id, oldx, oldy, x, y) {
        const sprite = this.findObjectSprite(id);
        if (sprite == null)
            return;
        this.addAction(new MoveAction(sprite, {
            fromX: this.tileW * oldx,
            fromY: -this.tileH * oldy,
            toX: this.tileW * x,
            toY: -this.tileH * y,
            time: 500
        }));
    }
    onDelete(id) {
        const sprite = this.findObjectSprite(id);
        if (sprite != null)
            this.container.removeChild(sprite);
    }
    onCreate(object) {
        this.container.addChild(this.createObjectSprite(
            object.id,
            object.look,
            object.x * this.tileW,
            -object.y * this.tileH
        ));
    }
    onHit(dstId, damage) {
        console.log(`on hit: ${dstId} ${damage}`);
        const dstObject = this.state.findObjectById(dstId);
        if (dstObject == null)
            return;
        const text = new PIXI.Text("" + damage, {
            fontSize: "16px",
            fill: "#a00"
        });
        text.anchor.set(0.5);
        this.container.addChild(text);
        const action = new HitAction(this.container,
            text,
            dstObject.x * this.tileW,
            -dstObject.y * this.tileH - 16
        );
        this.addAction(action);
        action.perform(0);
        // text.x = dstObject.x * this.tileW;
        // text.y = -dstObject.y * this.tileH;
    }
    onChangeLook(id, look) {
        const sprite = this.findObjectSprite(id);
        if (sprite != null) {
            sprite.texture = this.textures[look];
        }
    }
}