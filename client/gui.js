import * as PIXI from "pixi.js";

export class Gui {
    constructor(app, game, params) {
        this.app = app;
        this.game = game;
        this.images = {};
        this.loginCallback = params.loginCallback || (() => { });
        this.scale = params.scale || 1.5;
        this.textures = {};

        PIXI.loader
            .load((loader, resources) => {
                this.addTextures(resources.toolbar.texture);
                this.addFPS();
                this.addLoginGui();
                this.setLoginVisible(true);
                this.addPanel();
            });
    }
    addTextures(toolbar) {
        this.addTexture("SKIP", toolbar, 0, 0, 22, 24);
        this.addTexture("FIND", toolbar, 22, 0, 22, 24);
        this.addTexture("ASK", toolbar, 44, 0, 22, 24);
        this.addTexture("TURN_RED", toolbar, 110, 0, 25, 24);
        this.addTexture("TURN_GREEN", toolbar, 135, 0, 25, 24);
        this.addTexture("TURN_YELLOW", toolbar, 160, 0, 25, 24);
        this.addTexture("STATS", toolbar, 185, 0, 120, 24);
    }
    addTexture(name, texture, x, y, width, height) {
        const result = new PIXI.Texture(texture, new PIXI.Rectangle(x, y, width, height));
        this.textures[name] = result;
    }
    addPanel() {
        const container = new PIXI.Container();
        container.visible = false;
        this.panelContainer = container;

        const skip = this.createTextureButton("SKIP", () => {
            this.game.skipTurn();
        });
        skip.x = 0;
        skip.y = 0;
        container.addChild(skip);

        const find = this.createTextureButton("FIND", () => {
            this.game.skipTurn();
        });
        find.x = skip.width;
        find.y = 0;
        container.addChild(find);

        container.x = this.app.renderer.width / 2 - 22 * this.scale | 0;
        container.y = this.app.renderer.height - 24 * this.scale | 0;
        container.scale.set(this.scale);

        this.app.stage.addChild(container);

        this.addStatsGui();
    }
    createTextureButton(name, callback) {
        const button = new PIXI.Sprite(this.textures[name]);
        button.interactive = true;
        button.on("pointerdown", callback);
        return button;
    }
    createTurnButton(state, object) {
        const renderer = this.game.renderer;
        const container = new PIXI.Container();

        let background;
        if (object.id === state.playerId) {
            background = new PIXI.Sprite(this.textures.TURN_GREEN);
        } else if (object.look === "PLAYER") {
            background = new PIXI.Sprite(this.textures.TURN_YELLOW);
        } else {
            background = new PIXI.Sprite(this.textures.TURN_RED);
        }
        container.addChild(background);

        const sprite = new PIXI.Sprite(renderer.textures[object.look]);
        sprite.x = 3;
        sprite.y = 3;
        sprite.scale.set(0.5);
        container.addChild(sprite);

        return container;
    }
    updateTurns(state) {
        if (this.turnsContainer == null) {
            this.turnsContainer = new PIXI.Container();
            this.turnsContainer.visible = true;
            this.turnsContainer.x = this.app.renderer.width - 25 * this.scale | 0;
            this.turnsContainer.y = this.app.renderer.height - 24 * this.scale;
            this.turnsContainer.scale.set(this.scale);
            this.app.stage.addChild(this.turnsContainer);
        } else {
            this.turnsContainer.removeChildren();
        }
        const turnIds = state.turns;
        let offset = 0;
        for (const id of turnIds) {
            const obj = state.findObjectById(id);
            if (obj != null) {
                const turnButton = this.createTurnButton(state, obj);
                turnButton.x = 0;
                turnButton.y = offset;
                offset -= 20 * this.scale;
                this.turnsContainer.addChild(turnButton);
            }
        }
    }
    createInput(x, y) {
        const inputStyle = {
            fontSize: "12pt",
            padding: "10px",
            width: "200px",
            fill: "#333"
        };
        const boxStyle = {
            default: { fill: 0xE8E9F3, rounded: 16, stroke: { color: 0xCBCEE0, width: 4 } },
            focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0xABAFC6, width: 4 } },
            disabled: { fill: 0xDBDBDB, rounded: 16 }
        };
        const input = new PIXI.TextInput(inputStyle, boxStyle);
        input.x = x;
        input.y = y;
        return input;
    }
    addFPS() {
        const fpsText = new PIXI.Text("", {
            fill: "#fff"
        });
        this.fps = fpsText;
        this.app.stage.addChild(fpsText);
        let elapsed = 0;
        this.app.ticker.add(() => {
            const fps = this.app.ticker.FPS;
            elapsed += this.app.ticker.elapsedMS;
            if (elapsed >= 500) {
                fpsText.text = ("" + fps).substring(0, 5);
                elapsed = 0;
            }
        });
    }
    createButton(x, y, text, callback) {
        const button = new PIXI.Text(text, {
            fontSize: "14pt",
            fill: "#fff"
        });
        button.x = x;
        button.y = y;
        button.interactive = true;
        button.buttonMode = true;
        button.on("pointerdown", callback);
        return button;
    }
    addLoginGui() {
        const loginGuiContainer = new PIXI.Container();
        this.loginGuiContainer = loginGuiContainer;
        loginGuiContainer.visible = false;

        const loginInput = this.createInput(0, 100);
        loginInput.text = "root";
        loginGuiContainer.addChild(loginInput);
        this.loginInput = loginInput;

        const passwordInput = this.createInput(0, 50);
        passwordInput.text = "root";
        loginGuiContainer.addChild(passwordInput);
        this.passwordInput = passwordInput;

        const button = this.createButton(100, 25, "login", () => {
            this.loginCallback(loginInput.text, passwordInput.text);
        });
        button.anchor.set(0.5);
        loginGuiContainer.addChild(button);

        loginGuiContainer.visible = false;
        this.app.stage.addChild(loginGuiContainer);
    }
    addStatsGui() {
        const statsContainer = new PIXI.Container();
        statsContainer.visible = false;
        this.statsContainer = statsContainer;

        const background = new PIXI.Sprite(this.textures.STATS);
        statsContainer.addChild(background);

        const textConfig = {
            fontSize: "14px",
            fill: "#000"
        };

        const hpText = new PIXI.Text("-", textConfig);
        hpText.x = 20;
        hpText.y = 5;
        this.hpText = hpText;
        statsContainer.addChild(hpText);

        const mpText = new PIXI.Text("-", textConfig);
        mpText.x = 57;
        mpText.y = 5;
        this.mpText = mpText;
        statsContainer.addChild(mpText);

        const apText = new PIXI.Text("-", textConfig);
        apText.x = 100;
        apText.y = 5;
        this.apText = apText;
        statsContainer.addChild(apText);

        statsContainer.x = this.app.renderer.width / 2 - background.width / 2 | 0;
        statsContainer.y = this.app.renderer.height - 2 * 24 * this.scale;
        statsContainer.scale.set(this.scale);

        this.app.stage.addChild(statsContainer);
    }
    updateStats(state) {
        this.hpText.text = state.HP.toString();
        if (state.playerId === state.turns[0]) {
            this.mpText.text = state.MP.toString();
            this.apText.text = state.AP.toString();
        } else {
            console.log("redraw to -")
            this.mpText.text = "-";
            this.apText.text = "-";
        }
    }
    setLoginVisible(visible) {
        this.loginGuiContainer.visible = visible;
    }
    setFPSVisible(visible) {
        this.fps.visible = visible;
    }
    setPanelVisible(visible) {
        this.panelContainer.visible = visible;
        this.statsContainer.visible = visible;
    }
}