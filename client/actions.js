export class MoveAction {
    constructor(sprite, params) {
        this.sprite = sprite;
        this.id = params.id;
        this.fromX = params.fromX || 0;
        this.fromY = params.fromY || 0;
        this.toX = params.toX || 0;
        this.toY = params.toY || 0;
        this.time = params.time || 500;
        this.totalElapsed = 0;
    }
    perform(elapsed) {
        this.totalElapsed += elapsed;
        const t = this.totalElapsed / this.time;
        if (t <= 1) {
            this.sprite.x = this.fromX * (1 - t) + this.toX * t;
            this.sprite.y = this.fromY * (1 - t) + this.toY * t;
            return [this];
        } else {
            this.sprite.x = this.toX;
            this.sprite.y = this.toY;
            return [];
        }
    }
}

export class HitAction {
    constructor(container, textSprite, fromX, fromY) {
        this.container = container;
        this.textSprite = textSprite;
        this.fromX = fromX;
        this.fromY = fromY;
        this.totalElapsed = 0;
    }
    perform(elapsed) {
        this.totalElapsed += elapsed;
        const t = this.totalElapsed / 1000;
        if (t >= 0 && t < 0.3) {
            this.textSprite.x = this.fromX;
            this.textSprite.y = this.fromY * (1 - t) + (this.fromY - 32) * t;
            return [this];
        } else if (t >= 0.3 && t < 0.7) {
            const byte = 255 - (((t - 0.3) / 0.4 * 256) | 0);
            const color = byte | (byte << 8) | (byte << 16);
            this.textSprite.tint = color;
            return [this];
        } else {
            this.container.removeChild(this.textSprite);
            return [];
        }
    }
}