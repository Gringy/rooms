const abilities = require("./abilities");
const utils = require("./utils");

class World {
	/**
	 * Creates an empty world
	 */
    constructor() {
        this.mapper = new ConnectionMapper(this);
        this._rooms = [];
        this.lastId = 1;
    }

    get connectionMapper() {
        return this.mapper;
    }

    get rooms() {
        return this._rooms;
    }

	/**
	 * Adds room to world
	 * @param {room.Room} room 
	 */
    addRoom(room) {
        this._rooms.push(room);
    }

    /**
     * @param {room.Room} value
     */
    set playerRoom(value) {
        this._playerRoom = value;
    }

    get playerRoom() {
        return this._playerRoom;
    }

	/**
	 * Calls function for every object in every room
	 * @param {(GameObject) => void} callback 
	 */
    forEachObject(callback) {
        this.forEachRoom((room) => {
            room.forEachObject(callback);
        });
    }

	/**
	 * Calls function for every object with ability abilityClass
	 * @param {abilities.Ability} abilityClass 
	 * @param {(GameObject) => void} callback 
	 */
    forEachAbilityObject(abilityClass, callback) {
        this.forEachObject((object) => {
            if (object.hasAbility(abilityClass))
                callback(object);
        })
    }

	/**
	 * Calls function for every Room in World
	 * @param {(Room) =>} callback 
	 */
    forEachRoom(callback) {
        for (const room of this.rooms) {
            callback(room);
        }
    }

	/**
	 * @returns {number} Next empty (free) id for GameObject
	 */
    newId() {
        return this.lastId++;
    }

	/**
	 * Starts ticking of a world every 500 msec
	 */
    async start() {
        console.log("world starts");
        //let tickTime = 1;
        while (true) {
            await utils.wait(300);
            //tickTime++;
            // this.forEachAbilityObject(abilities.TurnAbility, (object) => {
            //     const turnAbility = object.getAbility(abilities.TurnAbility);
            //     turnAbility.turn(tickTime);
            // });
            this.forEachRoom((room) => {
                room.tick();
            })
        }
    }
}

/**
 * Helper class to link inner connection with player's GameObject in World
 */
class ConnectionMapper {
    constructor(world) {
        this.world = world;
    }
	/**
	 * Links connection and object with this login
	 * @param {string} login 
	 * @param {net.Conn} conn 
	 */
    async map(login, conn) {
        console.log("connection: " + conn.address + " login: " + login);
        let player;
        this.world.forEachAbilityObject(abilities.UserAbility, (aPlayer) => {
            const userAbility = aPlayer.getAbility(abilities.UserAbility);
            if (userAbility.login === login)
                player = aPlayer;
        });
        if (player == null) {
            console.log("player with login " + login + " not found");
            return;
        }
        const userAbility = player.getAbility(abilities.UserAbility);
        userAbility.sender = conn;
        try {
            while (true) {
                const message = await conn.read();
                userAbility.message(JSON.parse(message));
            }
        } catch (e) {
            userAbility.sender = null;
            console.log(login + ": " + e);
            console.log(e.stack);
        }
    }
}

exports.World = World;
