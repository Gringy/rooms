class Type {
    constructor(name, passable) {
        this.name = name;
        this.passable = passable;
    }
}

const types = {};

function type(name, passable) {
    const type = new Type(name, passable);
    types[name] = type;
    return type;
}

type("WALL", false);
type("FLOOR", true);
type("GRASS", true);
type("UNDERGRASS", true);
type("STAIRS", true);
type("DOOR", true);

function find(name) {
    name = name.toUpperCase();
    return types[name];
}

exports.find = find;