const abs = require("./abilities");
const utils = require("./utils");

class Command {
    perform() {
        //
    }
    postPerform() {
        //
    }
    get MP() {
        return 0;
    }
    get AP() {
        return 0;
    }
    get time() {
        return 0;
    }
}
exports.Command = Command;

exports.MoveCommand = class MoveCommand extends Command {
    constructor(object, to) {
        super();
        this.object = object;
        this.to = to;
    }
    perform() {
        const moveAbility = this.object.getAbility(abs.MoveAbility);
        moveAbility.moveTo(...this.to);
    }
    getDistance() {
        return utils.distance(this.object.getAbility(abs.MoveAbility).getCoords(), this.to);
    }
    get MP() {
        return this.getDistance();
    }
    get AP() {
        return 0;
    }
    get time() {
        return this.getDistance();
    }
}

exports.HitCommand = class HitCommand extends Command {
    constructor(src, dst) {
        super();
        this.src = src;
        this.dst = dst;
    }
    perform() {
        const hitAbility = this.src.getAbility(abs.HitAbility);
        hitAbility.hit(this.dst);
    }
    get AP() {
        return 2;
    }
    get time() {
        return 3;
    }
}

exports.EndTurnCommand = class EndTurnCommand extends Command {
    constructor(object) {
        super();
        this.object = object;
    }
    perform() {
        this.object.room.endTurn();
    }
}

