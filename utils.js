/**
 * Directions enum
 */
const directions = ["UP", "RIGHT", "DOWN", "LEFT"];

/**
 * Returns random int in range [from, to)
 * @param {number} from 
 * @param {number} to 
 */
function randInt(from, to) {
    return from + ((Math.random() * (to - from)) | 0);
}

/**
 * Returns random element of array
 * @param {any[]} arr 
 */
function randElement(arr) {
    return arr[randInt(0, arr.length)];
}

/**
 * Generates two coordinates in range [from, to)
 * @param {number} from 
 * @param {number} to 
 * 
 * @returns {number[]}
 */
function randCoords(from, to) {
    return [randInt(from, to), randInt(from, to)];
}

/**
 * Generates two coordinates, x in [0, maxX), y in [0, maxY)
 * @param {number} maxX 
 * @param {number} maxY 
 * 
 * @returns {number[]}
 */
function pickCoords(maxX, maxY) {
    return [randInt(0, maxX), randInt(0, maxY)];
}

/**
 * Translates direction string to coordinates
 * @param {string} dir 
 * 
 * @returns {number[]}
 */
function directionToCoords(dir) {
    switch (dir) {
        case "UP":
            return [0, 1];
        case "RIGHT":
            return [1, 0];
        case "DOWN":
            return [0, -1];
        case "LEFT":
            return [-1, 0];
        default:
            throw new Exception("utils.directionToCoords: unknown direction: " + dir);
    }
}

/**
 * Returns reverse direction
 * @param {string} dir 
 * 
 * @returns {string}
 */
function backDir(dir) {
    switch (dir) {
        case "UP":
            return "DOWN";
        case "RIGHT":
            return "LEFT";
        case "DOWN":
            return "UP";
        case "LEFT":
            return "RIGHT";
        default:
            throw new Exception("utils.backDir: unknown direction: " + dir);
    }
}

/**
 * Checks if parameter is a direction
 * @param {string} dir 
 * 
 * @returns {boolean}
 */
function isDir(dir) {
    return directions.includes(dir);
}

/**
 * Print error with stacktrace
 * @param {Error} e Error to print
 */
function printError(e) {
    console.log(e.stack);
}

/**
 * 
 * @param {number} time time in msecs to wait
 * 
 * @returns {Promise<void>}
 */
function wait(time) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(), time);
    });
}

/**
 * 
 * @param {number[]} from 
 * @param {number[]} to 
 * @param {number} delim Distance delimiter
 */
function randVector(from, to, delim) {
    const [fx, fy] = from;
    const [tx, ty] = to;
    const dx = Math.min(Math.abs(tx - fx), delim / 2 | 0) * Math.sign(tx - fx);
    const dy = Math.min(Math.abs(ty - fy), delim / 2 | 0) * Math.sign(ty - fy);
    return [randInt(0, dx + Math.sign(dx)), randInt(0, dy + Math.sign(dy))];
}

/**
 * 
 * @param {number[]} from 
 * @param {number[]} to 
 */
function distance(from, to) {
    const [fx, fy] = from;
    const [tx, ty] = to;
    return Math.abs(tx - fx) + Math.abs(ty - fy);
}

/**
 * Returns a - b
 * @param {number[]} a 
 * @param {number[]} b 
 * 
 * @returns {number[]}
 */
function vectorSub(a, b) {
    const [ax, ay] = a;
    const [bx, by] = b;
    return [ax - bx, ay - by];
}

/**
 * Returns a + b
 * @param {number[]} a 
 * @param {number[]} b 
 * 
 * @returns {number[]}
 */
function vectorAdd(a, b) {
    const [ax, ay] = a;
    const [bx, by] = b;
    return [ax + bx, ay + by];
}

function length(vec) {
    return Math.abs(vec[0]) + Math.abs(vec[1]);
}

exports.directions = directions;
exports.randInt = randInt;
exports.randElement = randElement;
exports.randCoords = randCoords;
exports.isDir = isDir;
exports.printError = printError;
exports.directionToCoords = directionToCoords;
exports.pickCoords = pickCoords;
exports.backDir = backDir;
exports.wait = wait;
exports.randVector = randVector;
exports.distance = distance;
exports.vectorSub = vectorSub;
exports.vectorAdd = vectorAdd;
exports.length = length;
