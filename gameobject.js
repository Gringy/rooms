const abilities = require("./abilities");
const room = require("./room");
const world = require("./world");

/**
 * Represents game object - ability container
 */
class GameObject {
	/**
	 * 
	 * @param {world.World} world 
	 * @param {room.Room} room 
	 */
    constructor(world, room) {
        this.abilities = [];
        this.world = world;
        this._room = room;
        this.id = world.newId();
    }

	/**
	 * 
	 * @param  {...abilities.Ability} abilities
	 * 
	 * @returns {GameObject} Self. 
	 */
    addAbility(...abilities) {
        for (const a of abilities)
            this.abilities.push(a);
        return this;
    }

	/**
	 * 
	 * @param {new () => abilities.Ability} abilityClass 
	 * 
	 * @returns {abilities.Ability}
	 */
    getAbility(abilityClass) {
        for (const a of this.abilities)
            if (a instanceof abilityClass)
                return a;
        return null;
    }

	/**
	 * Check if this object has ability
	 * @param {new() => abilities.Ability} abilityClass
	 * 
	 * @returns {boolean}
	 */
    hasAbility(abilityClass) {
        for (const a of this.abilities)
            if (a instanceof abilityClass)
                return true;
        return false;
    }

    get room() {
        return this._room;
    }

	/**
	 * @param {room.Room} value
	 */
    set room(value) {
        this._room = value;
        if (this.hasAbility(abilities.UserAbility)) {
            this.getAbility(abilities.UserAbility).initMessage();
        }
    }

    isTurning() {
        return this.room.isTurning(this);
    }

	/**
	 * Generates an object representation of GameObject to send to client
	 * 
	 * @returns {Object}
	 */
    serialize() {
        const result = {}
        result.id = this.id;

        const moveAbility = this.getAbility(abilities.MoveAbility);
        if (moveAbility != null) {
            result.x = moveAbility.x;
            result.y = moveAbility.y;
        }

        const lookAbility = this.getAbility(abilities.LookAbility);
        if (lookAbility != null) {
            result.look = lookAbility.getLook();
        }

        return result;
    }

    /**
     * @returns {boolean}
     */
    isDead() {
        const hpAbility = this.getAbility(abilities.HPAbility);
        if (hpAbility == null)
            return false;
        return hpAbility.HP === 0;
    }
}

exports.GameObject = GameObject;