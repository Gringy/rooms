const abilities = require("./abilities");
const turn = require("./turn");
const go = require("./gameobject");
const room = require("./room");

/**
 * Implements room
 */
class Room {
	/**
	 * 
	 * @param {number} width 
	 * @param {number} height
	 */
    constructor(width, height) {
        this._objects = [];
        this._nodes = [];
        this._neighbours = {};
        this._turns = [];
        this._width = width;
        this._height = height;
        for (let i = 0; i < width * height; i++)
            this._nodes.push(null);
    }

	/**
	 * Adds Object to Room and announce this event to everybody
	 * @param {go.GameObject} object 
	 */
    addObject(object) {
        this.objects.push(object);
        if (object.hasAbility(abilities.TurnAbility))
            this._turns.push(new turn.Turn(object));
        this.announceAddObject(object);
    }

	/**
	 * Removes GameObject from Room and announce this event to everybody
	 * @param {go.GameObject} object 
	 */
    removeObject(object) {
        for (let index in this.objects)
            if (this._objects[index] === object) {
                this._objects.splice(index, 1);
                break;
            }

        for (let i = 0; i < this._turns.length; i++)
            if (this._turns[i].object === object) {
                this._turns.splice(i, 1);
                break;
            }
        this.announceLeave(object);
    }

    /**
     * 
     * @param {go.GameObject} object 
     */
    isObjectHere(object) {
        for (const obj of this.objects) {
            if (obj === object)
                return true;
        }
        return false;
    }

    /**
     * Adds object's turn to room
     * @param {turn.Turn} turn 
     */
    addTurn(turn) {
        this._turns.push(turn);
    }

    isTurning(object) {
        if (this._turns.length == 0)
            return false;
        return this._turns[0].object.id === object.id;
    }

    endTurn() {
        const turn = this._turns.shift();
        this._turns.push(turn.next());
        this.announceTurnEnd();
    }

    /**
     * Returns id's of object in order of it's turns
     * 
     * @returns {number[]}
     */
    getTurnIds() {
        const result = [];
        for (const turn of this._turns)
            result.push(turn.object.id);
        return result;
    }

	/**
	 * @returns {go.GameObject[]}
	 */
    get objects() {
        return this._objects;
    }

	/**
	 * @returns {Node[]}
	 */
    get nodes() {
        return this._nodes;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

	/**
	 * Gets a node in (x, y)
	 * @param {number} x 
	 * @param {number} y 
	 * 
	 * @returns {Node}
	 */
    getNode(x, y) {
        if (x < 0 || y < 0 || x >= this.width || y >= this.height)
            throw new RangeError("Room.getNode: coords is out of range");
        return this._nodes[y * this.width + x];
    }

	/**
	 * Sets a node in (x, y)
	 * @param {number} x 
	 * @param {number} y 
	 * @param {Node} node 
	 */
    setNode(x, y, node) {
        if (x < 0 || y < 0 || x >= this.width || y >= this.height)
            throw new RangeError("Room.setNode: coords is out of range");
        this._nodes[y * this.width + x] = node;
    }

	/**
	 * Sets the neighbour of room in specified direction
	 * @param {string} dir 
	 * @param {room.Room} room 
	 */
    addNeighbour(dir, room) {
        this.neighbours[dir] = room;
    }

	/**
	 * Returns neighbour of room in specified direction
	 * @param {string} dir 
	 * 
	 * @returns {room.Room}
	 */
    getNeighbour(dir) {
        return this.neighbours[dir];
    }

    get neighbours() {
        return this._neighbours;
    }

    forEachObject(callback) {
        for (const object of this.objects) {
            callback(object);
        }
    }

    findObjectWithAbility(ability) {
        for (const object of this.objects) {
            if (object.hasAbility(ability)) {
                return object;
            }
        }
        return null;
    }

    findObjectsWithAbility(ability) {
        const result = [];
        for (const object of this.objects) {
            if (object.hasAbility(ability)) {
                result.push(object);
            }
        }
        return result;
    }

    /**
     * 
     * @param {number} id 
     */
    findObjectById(id) {
        for (const object of this.objects) {
            if (object.id == id)
                return object;
        }
        return null;
    }

	/**
	 * Performs tick
	 */
    tick() {
        if (this._turns.length == 0) return;
        const currentTurn = this._turns[0];
        currentTurn.tick();
    }

	/**
	 * Announses message for every object in room
	 * @param {any} msg Object to announce
	 * 
	 * @returns {void}
	 */
    announce(msg) {
        this.forEachObject((object) => {
            if (object.hasAbility(abilities.UserAbility)) {
                const userAbility = object.getAbility(abilities.UserAbility);
                userAbility.send(msg);
            }
        });
    }

    announceAddObject(object) {
        this.announce({
            type: "add_object",
            turns: this.getTurnIds(),
            object: object.serialize()
        });
    }

    announceTurnEnd() {
        this.announce({
            type: "turn_end",
            turns: this.getTurnIds()
        });
    }

    announceLeave(object) {
        this.announce({
            type: "leave",
            id: object.id,
            turns: this.getTurnIds()
        });
    }

    announceMove(object, from, to, way) {
        this.announce({
            type: "move",
            id: object.id,
            from: from,
            to: to,
            way: way
        });
    }

    announceHit(src, dst, damage) {
        this.announce({
            type: "hit",
            src: src.id,
            dst: dst.id,
            damage: damage
        });
    }

    announceChange(object, look) {
        this.announce({
            type: "change",
            id: object.id,
            look: look
        });
    }

	/**
	 * Generates an object representation of room to send to client
	 * @returns {Object}
	 */
    serialize() {
        const result = {};
        result.width = this.width;
        result.height = this.height;
        result.nodes = [];
        result.turns = this.getTurnIds();
        for (const node of this.nodes)
            if (node != null)
                result.nodes.push(node.type);
            else
                result.nodes.push(null);
        result.objects = [];
        for (const object of this.objects) {
            const serialized = object.serialize();
            if (serialized != null)
                result.objects.push(serialized);
        }
        return result;
    }
}

/**
 * Represents a cell of room
 */
class Node {
	/**
	 * 
	 * @param {string} type Type name of node. All types in types.js
	 * @param {string} action Action when object is moved to this node
	 */
    constructor(type, action) {
        this._type = type;
        this._action = action;
    }
    get type() {
        return this._type;
    }
    get action() {
        return this._action;
    }
}

exports.Room = Room;
exports.Node = Node;