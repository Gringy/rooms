const world = require("./world");
const room = require("./room");
const utils = require("./utils");
const abilities = require("./abilities");
const types = require("./types");
const go = require("./gameobject");
const strategy = require("./strategy");

class NodeChecker {
    get wall() {
        return "WALL";
    }
    get floor() {
        return "FLOOR";
    }
}

class GrassedChecker extends NodeChecker {
    constructor(grass) {
        super();
        this.grass = grass;
    }
    get floor() {
        if (Math.random() < this.grass)
            return "GRASS";
        else if (Math.random() < this.grass)
            return "UNDERGRASS";
        else
            return "FLOOR";
    }
}

class ObjectCreator {
    constructor(world) {
        this.world = world;
    }
    getPlayer(room, login, x, y) {
        const player = new go.GameObject(this.world, room);
        player.addAbility(
            new abilities.UserAbility(player, login),
            new abilities.MoveAbility(player, x, y),
            new abilities.TurnAbility(player, new strategy.PlayerStrategy(player)),
            new abilities.LookAbility(player, "PLAYER"),
            new abilities.HPAbility(player, 50),
            new abilities.HitAbility(player, 7)
        );
        return player;
    }
    getCrab(room, x, y) {
        const crab = new go.GameObject(this.world, room);
        crab.addAbility(
            new abilities.MoveAbility(crab, x, y),
            new abilities.TurnAbility(crab, new strategy.CrabStrategy(crab), {
                maxMP: 4
            }),
            new abilities.LookAbility(crab, "CRAB"),
            new abilities.HPAbility(crab, 10),
            new abilities.HitAbility(crab, 5)
        );
        return crab;
    }
    getTree(room, x, y) {
        const tree = new go.GameObject(this.world, room);
        tree.addAbility(
            new abilities.MoveAbility(tree, x, y),
            new abilities.LookAbility(tree, "TREE")
        );
        return tree;
    }
}

class RoomGenerator {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.room = new Array(width * height);
    }
    generateRoom() {
        //
    }
    setCell(x, y, type) {
        this.room[y * this.width + x] = type;
    }
    getCell(x, y) {
        return this.room[y * this.width + x];
    }
}

class BoxRoomGenerator extends RoomGenerator {
    constructor(width, height, checker) {
        super(width, height);
        this.checker = checker;
    }
    generateRoom() {
        // set walls
        for (let x = 0; x < this.width; x++) {
            this.setCell(x, 0, this.checker.wall);
            this.setCell(x, this.height - 1, this.checker.wall);
        }
        for (let y = 0; y < this.height; y++) {
            this.setCell(0, y, this.checker.wall);
            this.setCell(this.width - 1, y, this.checker.wall);
        }
        // set floor
        for (let x = 1; x < this.width - 1; x++)
            for (let y = 1; y < this.height - 1; y++)
                this.setCell(x, y, this.checker.floor);
    }
}

class CellularRoomGenerator extends RoomGenerator {
    constructor(width, height, params) {
        super(width, height);
        this.map = [];
        this.newMap = new Array(width * height);

        params = params || {};
        this.start = params.start || 0.3;
        this.checker = params.checker || new NodeChecker();
        this.birthLimit = params.birthLimit || 4;
        this.deathLimit = params.deathLimit || 3;
        this.steps = params.steps || 4;

        for (let i = 0; i < width * height; i++)
            this.map.push(Math.random() < this.start);
    }
    getMap(x, y) {
        return this.map[y * this.width + x];
    }
    setMap(x, y, bool) {
        this.map[y * this.width + x] = bool;
    }
    setNewMap(x, y, bool) {
        this.newMap[y * this.width + x] = bool;
    }
    countAlive(x, y) {
        let count = 0;
        for (let i = -1; i <= 1; i++)
            for (let j = -1; j <= 1; j++) {
                const nx = x + i;
                const ny = y + j;
                if (i === 0 && y === 0) {
                    continue;
                } else if (nx < 0 || ny < 0 || nx >= this.width || ny >= this.height) {
                    count++;
                } else if (this.getMap(nx, ny)) {
                    count++;
                }
            }
        return count;
    }
    doSimulationStep() {
        for (let x = 0; x < this.width; x++)
            for (let y = 0; y < this.height; y++) {
                const nbs = this.countAlive(x, y);
                if (this.getMap(x, y)) {
                    if (nbs < this.deathLimit)
                        this.setNewMap(x, y, false);
                    else
                        this.setNewMap(x, y, true);
                } else {
                    if (nbs > this.birthLimit)
                        this.setNewMap(x, y, true);
                    else
                        this.setNewMap(x, y, false);
                }
            }
        this.swapMaps();
    }
    swapMaps() {
        [this.newMap, this.map] = [this.map, this.newMap];
    }
    generateRoom() {
        for (let i = 0; i < this.steps; i++)
            this.doSimulationStep();
        for (let x = 0; x < this.width; x++)
            for (let y = 0; y < this.height; y++)
                if (this.getMap(x, y))
                    this.setCell(x, y, this.checker.wall);
                else {
                    this.setCell(x, y, this.checker.floor);
                    // if (Math.random() < this.grass)
                    //     this.setCell(x, y, "GRASS");
                    // else if (Math.random() < this.grass)
                    //     this.setCell(x, y, "UNDERGRASS");
                    // else
                    //     this.setCell(x, y, "FLOOR");
                }
    }
}

class RoomDescription {
    constructor(params) {
        this.width = params.width || 10;
        this.height = params.height || 10;
        this.logins = params.logins || [];
        this.trees = params.trees || 0;
        this.crabs = params.crabs || 0;
        this.type = params.type || "box";
        this.grassed = !!params.grassed;
        this.playerRoom = !!params.playerRoom;
        this.coords = params.coords;
        this.links = {};
    }
}

class WorldDescription {
    constructor() {
        this.rooms = [];
    }
    getRoomDescription(x, y) {
        for (const room of this.rooms) {
            const [rx, ry] = room.coords;
            if (rx === x && ry === y)
                return room;
        }
        return null;
    }
    /**
     * 
     * @param {RoomDescription} desc 
     */
    addRoomDescription(desc) {
        const [x, y] = desc.coords;
        for (let i = 0; i < this.rooms.length; i++) {
            const [rx, ry] = this.rooms[i].coords;
            if (rx === x && ry === y) {
                this.rooms[i] = desc;
                return;
            }
        }
        this.rooms.push(desc);
    }
}

class WorldGenerator {
    /**
     * 
     * @param {WorldDescription} desc 
     */
    constructor(worldDescription) {
        this.worldDescription = worldDescription;
    }

    generateWorld() {
        this.world = new world.World();
        for (const desc of this.worldDescription.rooms)
            this.generateRoom(desc);
        this.generateLinks();
        return this.world;
    }

    generateLinks() {
        for (const desc of this.worldDescription.rooms) {
            const [x, y] = desc.coords;
            // right
            const rightDesc = this.worldDescription.getRoomDescription(x + 1, y);
            if (rightDesc != null)
                this.generateLink(desc, rightDesc, "RIGHT");
            // left
            const leftDesc = this.worldDescription.getRoomDescription(x - 1, y);
            if (leftDesc != null)
                this.generateLink(desc, leftDesc, "LEFT");
            // up
            const upDesc = this.worldDescription.getRoomDescription(x, y + 1);
            if (upDesc != null)
                this.generateLink(desc, upDesc, "UP");
            // down
            const downDesc = this.worldDescription.getRoomDescription(x, y - 1);
            if (downDesc != null)
                this.generateLink(desc, downDesc, "DOWN");
        }
    }

    generateLink(desc, otherDesc, dir) {
        desc.room.addNeighbour(dir, otherDesc.room);
        const newRoom = desc.room;
        switch (dir) {
            case "LEFT":
                this.createJump(newRoom, 0, utils.randInt(1, desc.height - 1), "LEFT");
                break;
            case "RIGHT":
                this.createJump(newRoom, desc.width - 1, utils.randInt(1, desc.height - 1), "RIGHT");
                break;
            case "DOWN":
                this.createJump(newRoom, utils.randInt(1, desc.width - 1), 0, "DOWN");
                break;
            case "UP":
                this.createJump(newRoom, utils.randInt(1, desc.width - 1), desc.height - 1, "UP");
                break;
            default:
                throw new Error("Unknown direction");
        }
    }

    generateCoords(room) {
        return [0, 0];
    }

    createJump(where, x, y, action) {
        where.setNode(x, y, new room.Node("DOOR", action));
    }

    generateRoom(desc) {
        const objectCreator = new ObjectCreator(this.world);
        const newRoom = new room.Room(desc.width, desc.height);
        desc.room = newRoom;
        let roomGenerator;
        const checker = desc.grassed ? new GrassedChecker(0.1) : new NodeChecker();
        switch (desc.type) {
            case "dungeon":
                roomGenerator = new CellularRoomGenerator(desc.width, desc.height, {
                    checker: checker
                });
                break;
            case "box":
                roomGenerator = new BoxRoomGenerator(desc.width, desc.height, checker);
                break;
            default:
                throw new Error(`WorldGenerator.generateRoom: unknown type of room: ${desc.type}`);
        }
        roomGenerator.generateRoom();

        // add nodes
        for (let x = 0; x < desc.width; x++)
            for (let y = 0; y < desc.height; y++)
                newRoom.setNode(x, y, new room.Node(roomGenerator.getCell(x, y), null));

        // add players
        if (desc.logins != null)
            for (const login of desc.logins) {
                newRoom.addObject(objectCreator.getPlayer(newRoom, login, utils.randInt(1, desc.width - 1), utils.randInt(1, desc.height - 1)));
            }

        // set playerRoom
        if (desc.playerRoom)
            this.world.playerRoom = newRoom;

        // add trees
        for (let i = 0; i < desc.trees; i++)
            newRoom.addObject(objectCreator.getTree(newRoom, utils.randInt(1, desc.width - 1), utils.randInt(1, desc.height - 1)));

        // add crabs
        for (let i = 0; i < desc.crabs; i++)
            newRoom.addObject(objectCreator.getCrab(newRoom, utils.randInt(1, desc.width - 1), utils.randInt(1, desc.height - 1)));

        //

        this.world.addRoom(newRoom);
    }
}

/**
 * 
 * @param {string[]} logins 
 */
function testWorld(logins) {
    const worldDescription = new WorldDescription();
    worldDescription.addRoomDescription(new RoomDescription({
        width: 15,
        height: 15,
        logins: logins,
        trees: 5,
        crabs: 0,
        playerRoom: true,
        coords: [0, 0]
    }));
    worldDescription.addRoomDescription(new RoomDescription({
        width: 15,
        height: 15,
        trees: 5,
        crabs: 3,
        grassed: true,
        coords: [1, 0]
    }));
    const worldGenerator = new WorldGenerator(worldDescription);
    return worldGenerator.generateWorld();
}
exports.testWorld = testWorld;

function normWorld(logins) {
    const worldDescription = new WorldDescription();

    worldDescription.addRoomDescription(new RoomDescription({
        width: 10,
        height: 10,
        type: "box",
        logins: logins,
        playerRoom: true,
        coords: [0, 0]
    }));

    for (let x = -2; x <= 2; x++)
        for (let y = -2; y <= 2; y++)
            if (x != 0 || y != 0) {
                const crabs = utils.randInt(1, 5);
                const trees = utils.randInt(5, 10);
                const type = utils.randElement(["box", "dungeon"]);
                const width = utils.randInt(10, 20);
                const height = utils.randInt(10, 20);
                const grassed = Math.random() < 0.5;
                worldDescription.addRoomDescription({
                    width: width,
                    height: height,
                    coords: [x, y],
                    crabs: crabs,
                    trees: trees,
                    type: type,
                    grassed: grassed
                });
            }

    const worldGenerator = new WorldGenerator(worldDescription);
    return worldGenerator.generateWorld();
}
exports.normWorld = normWorld;
