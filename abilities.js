const commands = require("./commands");
const utils = require("./utils");
const types = require("./types");
const go = require("./gameobject");
const net = require("./net");
const turn = require("./turn");

class Ability {
    /**
     * 
     * @param {go.GameObject} object
     */
    constructor(object) {
        this._object = object;
    }

    get object() {
        return this._object;
    }

    get room() {
        return this._object.room;
    }

    getAbility(abilityClass) {
        return this._object.getAbility(abilityClass);
    }

    isTurning() {
        return this.room.isTurning(this.object);
    }

    ifPlayer(callback) {
        const userAbility = this.getAbility(UserAbility);
        if (userAbility != null)
            callback(userAbility);
    }
}
exports.Ability = Ability;

class UserAbility extends Ability {
    /**
     * 
     * @param {go.GameObject} object
     * @param {string} login 
     */
    constructor(object, login) {
        super(object);
        this.login = login;
    }
    /**
     * @param {net.Conn} value
     */
    set sender(value) {
        this._sender = value;
        if (value != null) {
            this.initMessage();
        }
    }
    initMessage() {
        this.send({
            type: "init",
            id: this.object.id,
            room: this.object.room.serialize(),
        });
    }
    statsMessage(mp, ap, hp) {
        this.send({
            type: "stats",
            MP: mp,
            AP: ap,
            HP: hp
        });
    }
    hpMessage(hp) {
        this.send({
            type: "stats",
            HP: hp
        });
    }
    get sender() {
        return this._sender;
    }

    popCommand() {
        const command = this.command;
        this.command = null;
        return command;
    }

    isLogined() {
        return this.sender != null;
    }

    message(obj) {
        if (obj == null || obj.type == null)
            return;
        if (!this.object.isTurning())
            return;
        switch (obj.type) {
            case "move_to": {
                if (obj.dx == null || obj.dy == null || typeof obj.dx != "number" || typeof obj.dy != "number")
                    return;
                const moveAbility = this.getAbility(MoveAbility);
                this.command = new commands.MoveCommand(this, [moveAbility.x + obj.dx, moveAbility.y + obj.dy]);
                break;
            }
            case "end_turn": {
                this.command = new commands.EndTurnCommand(this);
                break;
            }
            case "hit": {
                if (obj.id == null || typeof obj.id != "number")
                    return;
                const dst = this.object.room.findObjectById(obj.id);
                if (dst == null)
                    return;
                this.command = new commands.HitCommand(this.object, dst);
                break;
            }
            default:
                console.log("UserAbility.message: unknown command: " + obj.type);
                return;
        }
    }
    send(msg) {
        if (this.sender != null)
            this.sender.write(JSON.stringify(msg));
    }

    recurrect() {
        const playerRoom = this.object.world.playerRoom;

        this.getAbility(MoveAbility).setCoords([utils.randInt(1, playerRoom.width - 1), utils.randInt(1, playerRoom.height - 1)]);
        this.getAbility(LookAbility).look = "PLAYER";

        const hpAbility = this.getAbility(HPAbility);
        hpAbility.setHP(hpAbility.maxHP);
        this.hpMessage(hpAbility.HP);

        this.room.removeObject(this.object);
        playerRoom.addObject(this.object);
        this.object.room = playerRoom;
        // this.initMessage();


    }
}
exports.UserAbility = UserAbility;

class MoveAbility extends Ability {
    /**
     * 
     * @param {go.GameObject} object
     * @param {number} x 
     * @param {number} y 
     */
    constructor(object, x, y) {
        super(object);
        this.x = x;
        this.y = y;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    moveTo(x, y) {
        const room = this.object.room;
        if (x < 0 || x >= room.width || y < 0 || y >= room.height)
            return;
        const node = room.getNode(x, y);
        if (node == null || !types.find(node.type).passable)
            return;
        const action = node.action;
        if (action != null && utils.isDir(action)) {
            this.changeRoom(action)
            return;
        }
        // room.announceAction(new actions.MoveAction(this, [this.x, this.y], [x, y], "rlr"));
        room.announceMove(this.object, [this.x, this.y], [x, y], "rlr");
        this.x = x;
        this.y = y;
    }

    /**
     * 
     * @param {string} dir 
     */
    changeRoom(dir) {
        const currentRoom = this.object.room;
        const newRoom = currentRoom.getNeighbour(dir);
        if (newRoom == null)
            return;
        this.moveInNewRoom(dir, newRoom);
        currentRoom.removeObject(this.object);
        newRoom.addObject(this.object);
        this.object.room = newRoom;
    }

    /**
     * 
     * @param {string} dir 
     * @param {room.Room} room 
     */
    moveInNewRoom(dir, room) {
        let needAction = utils.backDir(dir);
        for (let x = 0; x < room.width; x++)
            for (let y = 0; y < room.height; y++) {
                const node = room.getNode(x, y);
                if (node != null && node.action === needAction) {
                    [this.x, this.y] = [x, y];
                    return;
                }
            }
        [this.x, this.y] = [room.width / 2 | 0, room.height / 2 | 0];
    }

    /**
     * 
     * @param {number} dx 
     * @param {number} dy 
     */
    move(dx, dy) {
        return this.moveTo(this.x + dx, this.y + dy, command);
    }

    /**
     * Getter for coordinates
     * @returns {number[]} pair of coordinates
     */
    getCoords() {
        return [this.x, this.y];
    }

    setCoords(coords) {
        [this.x, this.y] = coords;
    }

    getRelative(coords) {
        const [x, y] = coords;
        return [this.x + x, this.y + y];
    }
}
exports.MoveAbility = MoveAbility;

class HPAbility extends Ability {
    /**
     * 
     * @param {go.GameObject} object 
     * @param {number} maxHP 
     */
    constructor(object, maxHP) {
        super(object);
        this.maxHP = maxHP;
        this.HP = maxHP;
    }

    /**
     * 
     * @param {number} hp 
     */
    heal(hp) {
        this.setHP(this.HP + hp);
    }

    /**
     * 
     * @param {number} hp 
     */
    damage(hp) {
        this.setHP(this.HP - hp);
    }

    /**
     * 
     * @param {number} hp 
     */
    setHP(hp) {
        if (hp <= 0) {
            this.HP = 0;
            this.performDead();
        } else if (hp > this.maxHP)
            this.HP = this.maxHP;
        else
            this.HP = hp | 0;
        this.ifPlayer((userAbility) => {
            userAbility.hpMessage(this.HP);
        });
    }

    performDead() {
        console.log(`object #${this.object.id} is dead`);
        const lookAbility = this.getAbility(LookAbility);
        if (lookAbility != null) {
            lookAbility.setLook("CORPSE");
        }
    }
}
exports.HPAbility = HPAbility;

class HitAbility extends Ability {
    /**
     * 
     * @param {go.GameObject} object 
     * @param {number} damage 
     */
    constructor(object, damage) {
        super(object);
        this.damage = damage;
    }

    /**
     * 
     * @param {go.GameObject} dst 
     */
    hit(dst) {
        const hpAbility = dst.getAbility(HPAbility);
        hpAbility.damage(this.damage);
        this.object.room.announceHit(this.object, dst, this.damage);
    }
}
exports.HitAbility = HitAbility;

class TurnAbility extends Ability {
    /**
     * 
     * @param {go.GameObject} object 
     * @param {strategy.Strategy} strategy 
     */
    constructor(object, strategy, params) {
        super(object);

        this.strategy = strategy;

        params = params || {};

        this.maxAP = params.maxAP || 5;
        this.maxMP = params.maxMP || 5;
    }

    /**
     * 
     * @param {turn.Turn} currentTurn 
     */
    turn(currentTurn) {
        return this.strategy.getAction(currentTurn);
    }

    statsChanged(mp, ap) {
        const userAbility = this.getAbility(UserAbility);
        const hpAbility = this.getAbility(HPAbility);
        if (userAbility != null && hpAbility != null) {
            userAbility.statsMessage(mp, ap, hpAbility.HP);
        }
    }

    dead() {
        this.strategy.performDead();
    }
}
exports.TurnAbility = TurnAbility;

class LookAbility extends Ability {
    /**
     * 
     * @param {go.GameObject} object 
     * @param {string} look 
     */
    constructor(object, look) {
        super(object);
        this.look = look;
    }
    getLook() {
        return this.look;
    }
    setLook(look) {
        this.look = look;
        this.room.announceChange(this.object, look);
    }
}
exports.LookAbility = LookAbility;

