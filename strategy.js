const abilities = require("./abilities");
const commands = require("./commands");
const utils = require("./utils");

class Strategy {
    constructor(object) {
        this.object = object;
    }
    getAction(turn) {
        return null;
    }

    performDead() {
        //
    }
}
exports.Strategy = Strategy;

class PlayerStrategy extends Strategy {
    constructor(object) {
        super(object);
    }
    getAction(turn) {
        const userAbility = this.object.getAbility(abilities.UserAbility);
        if (userAbility.isLogined()) {
            const command = userAbility.popCommand();
            return command;
        } else {
            return new commands.EndTurnCommand(turn.object);
        }
    }
    performDead() {
        const userAbility = this.object.getAbility(abilities.UserAbility);
        userAbility.recurrect();
    }
}
exports.PlayerStrategy = PlayerStrategy;

function distance(object1, object2) {
    const moveAbility1 = object1.getAbility(abilities.MoveAbility);
    const moveAbility2 = object2.getAbility(abilities.MoveAbility);
    return utils.distance(moveAbility1.getCoords(), moveAbility2.getCoords());
}

function moveTo(moved, target, limit) {
    const from = moved.getAbility(abilities.MoveAbility).getCoords();
    const to = target.getAbility(abilities.MoveAbility).getCoords();
    let point = from;
    let d = utils.vectorSub(to, point);
    let limitLeft = limit;

    while (limitLeft > 0 && utils.distance(point, to) > 1) {
        let index = Math.random() > .5 ? 0 : 1;
        if (d[index] == 0)
            index = index == 0 ? 1 : 0;
        point[index] += Math.sign(d[index]);
        d = utils.vectorSub(to, point);
        limitLeft--;
    }

    return point;
}

class CrabStrategy extends Strategy {
    constructor(crab) {
        super(crab);
        this.player = null;
    }
    getAction(turn) {
        if (this.player == null || !this.object.room.isObjectHere(this.player) || this.player.isDead()) {
            this.player = null;
            const players = this.object.room.findObjectsWithAbility(abilities.UserAbility);
            if (players.length === 0)
                return new commands.EndTurnCommand(this.object);
            for (const player of players) if (!player.isDead()) {
                this.player = player;
                break;
            }
            if (this.player == null)
                return new commands.EndTurnCommand(this.object);
        }
        // if need to move to player
        if (distance(this.object, this.player) > 1) {
            if (turn.MP > 0) {
                // move to player
                const movePoint = moveTo(this.object, this.player, turn.MP);
                console.log(`moves to ${movePoint}`);
                if (movePoint != null)
                    return new commands.MoveCommand(this.object, movePoint);
                else
                    return new commands.EndTurnCommand(this.object);
            } else {
                // need to move, but no MP left
                return new commands.EndTurnCommand(this.object);
            }
        }
        // near player and can hit
        if (distance(this.object, this.player) <= 1 && turn.AP >= 2) {
            return new commands.HitCommand(this.object, this.player);
        }
        // no way
        return new commands.EndTurnCommand(this.object);
    }
    performDead() {
        //
        this.object.room.removeObject(this.object);
    }
}
exports.CrabStrategy = CrabStrategy;
