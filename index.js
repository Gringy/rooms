const netPackage = require("./net");
const generator = require("./generator");

const args = require("minimist")(process.argv.slice(2));

let testWorld = false;

if (args.world != null) {
    switch (args.world) {
        case "test":
            testWorld = true;
            break;
        case "norm":
            testWorld = false;
            break;
        default:
            console.log(`unknown option ${args.world}, use --world=norm or --world=test`);
            process.exit(1);
    }
}

let world;

if (testWorld)
    world = generator.testWorld(["root", "user"]);
else
    world = generator.normWorld(["root", "user"]);

// const world 
// = new WorldCreator({
//     width: 20,
//     height: 20,
//     rooms: testWorld ? 5 : 10,
//     mobs: testWorld ? 1 : 3,
//     logins: ["root", "user"]
// }).createWorld();

const net = new netPackage.Net(8086);

class Account {
    constructor(login, password) {
        this.login = login;
        this.password = password;
    }
}

const accounts = [
    new Account("root", "root"),
    new Account("user", "user")
];

const handleConnection = async (conn) => {
    try {
        const loginMessage = JSON.parse(await conn.read());
        let logined = false;
        for (let account of accounts) {
            if (account.login === loginMessage.login && account.password === loginMessage.password) {
                world.connectionMapper.map(account.login, conn);
                logined = true;
                break;
            }
        }
        if (!logined) {
            throw new Error("login failed: login=" + loginMessage.login + " pass=" + loginMessage.password);
        }
    } catch (error) {
        console.log(error.message);
        conn.close();
    }
}

const handleConnections = async () => {
    while (true) {
        const connection = await net.getConnection();
        handleConnection(connection);
    }
};

handleConnections();
world.start();
