const gulp = require("gulp");
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const webpack = require("webpack");
const webpackStream = require("webpack-stream");

function webpackConfig(mode) {
    return {
        output: {
            filename: 'game.js'
        },
        mode: mode,
        module: {
            rules: [
                {
                    test: /\.(js)$/,
                    exclude: /(node_modules)/,
                    loader: "babel-loader",
                    query: {
                        presets: ["@babel/preset-env"]
                    }
                }
            ]
        }
    }
}

gulp.task("client", () => {
    return gulp.src("./client/*.js")
        .pipe(webpackStream(webpackConfig("development")))
        .pipe(gulp.dest("./build"));
});

gulp.task("client_prod", () => {
    return gulp.src("./client/*.js")
        .pipe(webpackStream(webpackConfig("development")))
        .pipe(uglify())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest("./build"));
})

gulp.task("client_watch", () => {
    return gulp.watch("./client/*.js", gulp.series("client", "client_copy"));
});

gulp.task("client_copy", () => {
    return gulp.src(["./build/game.js", "./client/index.html"])
        .pipe(gulp.dest(process.env.HOME + "/www/game"));
})

gulp.task("default", gulp.series("client_watch"));